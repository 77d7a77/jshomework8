const inputField = document.getElementById("input");
inputField.addEventListener("blur", function () {
    let validValue = document.getElementById("input").value;
    if (validValue < 0){
        let showError = document.getElementById('span-error');
        showError.classList.remove("d-none");
        inputField.style.borderColor = "red"
        document.getElementById('input').value = "";
       }
    else {
    inputField.style.color = "green";
    inputField.style.borderColor = "black";
    inputField.before(createSpan());
    removeBtn();
    document.getElementById('span-error').classList.add("d-none");
    };
})
function createSpan() {
    let inputValue = document.getElementById("input").value;
    let newSpan = document.createElement("span");
    newSpan.innerHTML = `Текущая цена: ${inputValue}`
    newSpan.className = "span-price";
    newSpan.append(createBtn());
    return newSpan;
}

function createBtn() {
    let newBtn = document.createElement("button")
    newBtn.textContent = "x";
    newBtn.className = "btn";
    return (newBtn);
}

function removeBtn() {
    const remove = [...document.querySelectorAll(".btn")];
    remove.forEach((button) => {
        button.addEventListener("click", (event) => {
            event.target.closest(".span-price").remove();
            clearField();
        })
    })

};
function clearField() {
    document.getElementById("input").value = "";
}
// Уверен код можно упростить,пытался следовать логике операций!